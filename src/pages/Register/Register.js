import React from 'react'

export default function Register() {
  return (
    <div>
      <div style={{ textAlign: 'center' }}>
          <img src={ process.env.PUBLIC_URL + 'assets/images/user.png' } width="200"></img>
      </div>
      <form>
        <div class="form-group">
          <label for="exampleInputEmail1">Username</label>
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1"/>
        </div>
        <button type="submit" class="btn btn-success btn-block">Create</button>
      </form>
    </div>
  )
}
